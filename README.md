## How to install

- copy AdminerKeyboard.php to plugins directory
- create index.php file and [configure plugins](http://www.adminer.org/plugins/#use) if you don't have any other plugin before.
- add AdminerKeyboard to plugins array (AdminerKeyboard requires jQuery, Select2 and Mousetrap libraries), by default it tries to load them from `js` and `css` directories (put these directories to root folder, where `index.php` is). If you want to have these files elsewhere, you need to specify paths to javascript/css files when adding plugin

```php
<?php
function adminer_object() {
    ...

    $plugins = array(
        new AdminerKeyboard, //if you have js/css files in default directories, if you don't use line below
        new AdminerKeyboard(['path/to/jquery.js', 'path/to/select2.js', 'path/to/mousetrap.js'], ['path/to/select2.css']),
        ...
    );

    ...
}

```

Final file structure will be (if default paths are used):
```
- css
    - select2.min.css
- js
    - jquery.min.js
    - mousetrap.min.js
    - select2.min.js
- plugins
	- AdminerKeyboard.php
	- ...
	- plugin.php
- adminer.php
- index.php
```

## How to use

- press "o" to open "Go to table" dropdown where you can search for table (using simple fuzzy matching so "cusin" find "customers_invoices") and press enter to jump to selected table
- press "dd" on table's "Select data" page with content of table to delete all rows (it basically tick "whole result" checkbox and click "Delete" button, so you get default confirmation before action is taken)